import sqlite3 as lit

def main():
    try:
        conn = lit.connect("python_crud")
        cur = conn.cursor()
        tablecreate = "CREATE TABLE users(id INT AUTO_INCRIMENT,name TEXT,  email TEXT)"
        
        cur.execute(tablecreate)
        print("Table created...")
        
    except lit.Error as e:
        print("Unable to create Table!")
        
if __name__ == "__main__":
    main()