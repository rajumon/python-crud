import sqlite3 as lit

db = lit.connect("python_crud")
with db:
    user_id = 1
    cur = db.cursor()
    cur.execute("DELETE FROM users WHERE id = ?",(user_id,))
    db.commit()
    print("data deleted")