import sqlite3 as lit

db = lit.connect("python_crud")

with db:
    newname = "userNewName"
    id = 1
    
    cur = db.cursor()
    cur.execute("UPDATE users SET name = ? WHERE id = ?",(newname, id))
    db.commit()
    print("data updated")