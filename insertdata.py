import sqlite3 as lit

myuser =(
    (1,'Raju Mondal', 'raju.mondal@winexsoft.com'),
    (2,'Roni Mondal', 'roni.mondal@winexsoft.com'),
    (3,'Tanmoy Mondal', 'Tanmoy.mondal@winexsoft.com'),
)

db = lit.connect('python_crud')

with db:
    cur = db.cursor()
    cur.executemany("INSERT INTO  users VALUES(?,?,?)", myuser)
    print("data inserted")